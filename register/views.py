from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import UserRegisterForm


# Create your views here.

def login(request):
    return render(request,'login.html')

def logout(request):
    return render(request,'logout.html')
    
def signup(request):
    if request.method == "POST":
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request,f'Account created for {username} ')
            return redirect('/signup/')
    else :
        form = UserRegisterForm()
    return render(request,'signup.html', {'form' : form})