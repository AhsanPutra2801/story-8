from django.urls import path
from . import views

app_name = 'searchbox'

urlpatterns = [
    path('',views.booktable,name='booktable'),
    path('booksearched',views.booksearched,name='booksearched'),
]