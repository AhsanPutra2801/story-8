from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
import requests
import json
# Create your views here.

@login_required
def booktable(request):
    return render(request,'book_table.html')
    
@login_required
def booksearched(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data,safe=False)

