from django.test import TestCase,Client
from .views import booktable,booksearched
from django.urls import resolve
import requests
import json
# Create your tests here.

class TestAJAX(TestCase):
    def test_halaman_booktable_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,302)

    def test_func_booktable(self):
        find = resolve('/')
        self.assertEqual(find.func,booktable)

    # def test_template_booktable(self):
    #     response = Client().get('/')
    #     self.assertTemplateUsed(response,'book_table.html')

    # def test_json_in_booksearched(self):
    #     sample_input = "E"
    #     response = Client().get('/booksearched?q=' + sample_input)
    #     url = "https://www.googleapis.com/books/v1/volumes?q=" + sample_input
    #     ret = requests.get(url)
    #     data_from_url = json.loads(ret.content)
    #     data_from_booksearched = json.loads(response.content)
    #     self.assertEqual(data_from_url,data_from_booksearched)